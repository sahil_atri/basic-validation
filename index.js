/**
 * Created by sahil on 5/6/16.
 */

var async = require('async');
var typeFunctions = require('./typeFunctions');
var util = require('./util');

var validate = function (validateThis, againstThis, callback) {
    async.eachSeries(Object.keys(againstThis), function(key, callback) {
        if (validateThis[key] != "" && !validateThis[key]) {
            if (againstThis[key].isRequired) {
                return callback(key + " is required");
            }else {
                return callback(null);
            }
        }
        switch (againstThis[key].type) {
            case "Valid":
                if (!util.find(validateThis[key], againstThis[key].validData) && !util.find(validateThis[key], againstThis[key].allowData)) {
                    return callback(key + " can only be " + againstThis[key].validData.join() + " " + againstThis[key].allowData.join());
                }
                return callback(null);
            case "String":
                if (typeof validateThis[key] != "string") {
                    return callback(key + " must be a string");
                }
                if (util.find(validateThis[key], againstThis[key].allowData)) {
                    return callback(null);
                }
                if (validateThis[key] == "") {
                    return callback(key + " can not be empty string");
                }
                return callback(null);
            case "Number":
                if (typeof validateThis[key] != "number" && !util.find(validateThis[key], againstThis[key].allowData)) {
                    return callback(key + " must be a number");
                }
                return callback(null);
            case "Array":
                if (validateThis[key].constructor && validateThis[key].constructor === Array) {
                    if (!againstThis[key].Array.items) {
                        return callback(null);
                    }
                    async.eachSeries(Object.keys(validateThis[key]), function (index, callback) {
                        validate({item: validateThis[key][index]}, {item: againstThis[key].Array.items}, function (err) {
                            callback(err);
                        });
                    }, function (err) {
                        if (err) {
                            return callback(key + " fails because " + err);
                        } else {
                            callback(null);
                        }
                    });
                } else {
                    return callback(key + " must be an array");
                }
                break;

            case "Object":
                if (validateThis[key].constructor && validateThis[key].constructor === Object) {
                    if (!againstThis[key].Object.keys) {
                        return callback(null);
                    }
                    validate(validateThis[key], againstThis[key].Object.keys, function(err, result) {
                        if (err) {
                            return callback(key + " fails because " + err);
                        }
                        return callback(null);
                    });
                } else {
                    return callback(key + " must be an object");
                }
                break;

            default:
                callback("rule not found");

        }
    }, function(err) {
        if (err) {
            callback(err);
        }else {
            callback(err, validateThis);
        }
    });
};

var validationInfo = {
    isRequired: false,
    allowData: [],
    validData: [],
    type: "",
    String: {
        isString: false
    },
    Number: {
        isNumber: false
    },
    Array: {
        isArray: false,
        items: null
    },
    Object: {
        isObject: false,
        keys: null
    },
    string: typeFunctions.string,
    number: typeFunctions.number,
    allow: typeFunctions.allow,
    valid: typeFunctions.valid,
    array: typeFunctions.array,
    object: typeFunctions.object,
    validate: validate,
    items: typeFunctions.items,
    keys: typeFunctions.keys,
    required: typeFunctions.required
};


module.exports = validationInfo;