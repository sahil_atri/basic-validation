## Synopsis

This module applies basic validation on JSON data in dot cascade notation.

## Code Example



1. 

```javascript
var validation = require('basic-validation');

var againstThis = {
    title: validation.string().required(),
    author: validation.string().allow(['']),
    cost: validation.number().valid([1200, 1300]),
    publishers: validation.array().items(validation.object().keys({
        name: validation.string()
    }))
};

var validateThis = {
    title: "Conjuring",
    author: "MR X",
    cost: 1200
};


validation.validate(validateThis, againstThis, function(err, result) {
	console.log("err :-", err);
	console.log("result :-", result);
});


/*output will be
	err:- null
	result:- { title: 'Conjuring', author: 'MR X', cost: 1200 }
*/
```

2.

```javascript

var againstThis = {
    title: validation.string().required(),
    author: validation.string().allow(['']),
    cost: validation.number().valid([1200, 1300]),
    publishers: validation.array().items(validation.object().keys({
        name: validation.string()
    }))
};

var validateThis = {
    author: "MR X",
    cost: 1200
};


validation.validate(validateThis, againstThis, function(err, result) {
	console.log("err :-", err);
	console.log("result :-", result);
});


/*output will be
	err:- title is required 
	result:- undefined

*/
```




## Motivation

The module was created to apply validation on old node version where some functionality of 'joi' was not compatible; 
