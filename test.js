var validation = require('./index');





var againstThis = {
    title: validation.string().required(),
    author: validation.string().allow(['']),
    cost: validation.number().valid([1200, 1300]),
    publishers: validation.array().items(validation.object().keys({
        name: validation.string()
    }))
};


var validateThis = {
    author: "MR X",
    cost: 1200
};



validation.validate(validateThis, againstThis, function (err, result) {
    console.log(err, result)
});