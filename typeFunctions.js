/**
 * Created by sahil on 5/6/16.
 */

var util = require('./util');

exports.string = function() {
    if (this.Number.isNumber || this.Array.isArray || this.Object.isObject) {
        throw {error: "improper validation criteria", details: "can't be two things"}
    }
    var clone = util.getValidationClone(this);
    clone.String.isString = true;
    clone.type = "String";
    return clone;
};

module.exports.number = function() {
    if (this.String.isString || this.Array.isArray || this.Object.isObject) {
        throw {error: "improper validation criteria", details: "can't be two things"}
    }
    var clone = util.getValidationClone(this);
    clone.String.isString = true;
    clone.type = "Number";
    return clone;
};

module.exports.allow = function(allowData) {
    if (allowData.constructor !== Array) {
        throw {error: "improper allow array data", details: "allow data not an array"};
    }
    var clone = util.getValidationClone(this);
    clone.allowData = allowData;
    return clone;
};

module.exports.valid = function(validData) {
    if (validData.constructor !== Array) {
        throw {error: "improper valid array data", details: "valid data not an array"};
    }
    var clone = util.getValidationClone(this);
    clone.validData = validData;
    clone.type = "Valid";
    return clone;
};

module.exports.array = function () {
    if (this.String.isString || this.Number.isNumber || this.Object.isObject) {
        throw {error: "improper validation criteria", details: "can't be two things"}
    }
    var clone = util.getValidationClone(this);
    clone.Array.isArray = true;
    clone.type = "Array";
    return clone;
};
module.exports.object = function () {
    if (this.String.isString || this.Number.isNumber || this.Object.array) {
        throw {error: "improper validation criteria", details: "can't be two things"}
    }
    var clone = util.getValidationClone(this);
    clone.Object.isObject = true;
    clone.type = "Object";
    return clone;
};

module.exports.items = function (validationRule) {
    if (this.type != "Array") {
        throw {error: "not an array", details: "can't add items to something that is not array"}
    }
    if (!util.isValidationObject(validationRule)) {
        throw {error: "not a validation rule", details: "rule for items is not valid"};
    }
    var clone = util.getValidationClone(this);
    clone.Array.items = validationRule;
    return clone;
};

module.exports.required = function () {
    var clone = util.getValidationClone(this);
    clone.isRequired = true;
    return clone;
};

module.exports.keys = function (validationObject) {
    if (this.type != "Object") {
        throw {error: "not an object", details: "can't add keys to something that is not object"}
    }
    var clone = util.getValidationClone(this);
    clone.Object.keys = validationObject;
    return clone;
};
