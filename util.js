/**
 * Created by sahil on 5/6/16.
 */

var joi = {
    isRequired: "false",
    allowData: [],
    validData: [],
    type: "asd",
    String: {
        isString: false
    },
    Number: {
        isNumber: false
    },
    Array: {
        isArray: false,
        items: {}
    },
    Object: {
        isObject: false,
        keys: {}
    },
    string: "function",
    number: "function",
    allow: "function",
    valid: "function",
    array: "function",
    validate: "function",
    items: "function",
    keys: "function",
    required: "function"
};
var typeFunctions = require('./typeFunctions');

module.exports.isValidationObject = function (data) {
    if (!data) {
        return false;
    }
    var keys = Object.keys(data);

    for (var i = 0 ; i < keys.length ; i ++) {
        if (!!joi[keys[i]] == false) {
            console.log(joi[keys[i]], keys[i])
            return false;
        }
    }
    return true;
};


module.exports.find = function (value, data) {
    for (var i = 0 ; i < data.length ; i ++) {
        if (value === data[i]) {
            return true;
        }
    }
    return false;
};

exports.getValidationClone = function (validationObject) {
    var clone = JSON.parse(JSON.stringify(validationObject));
    clone.string = typeFunctions.string;
    clone.number = typeFunctions.number;
    clone.valid = typeFunctions.valid;
    clone.allow = typeFunctions.allow;
    clone.array = typeFunctions.array;
    clone.items = typeFunctions.items;
    clone.keys = typeFunctions.keys;
    clone.required = typeFunctions.required;
    return clone;
};